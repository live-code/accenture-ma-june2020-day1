import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './features/settings/settings.component';
import { HomeComponent } from './features/home/home.component';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

const routes = [
  {
    path: 'tvmaze',
    loadChildren: () => import('./features/tv-maze/tv-maze.module').then(m => m.TvMazeModule)
  },
  {
    path: 'uikit',
    loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule)
  },
  {
    path: 'catalog',
    loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule),
    canActivate: [AuthGuard]
  },
  { path: 'settings', component: SettingsComponent},
  { path: '', component: HomeComponent},
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: '**', redirectTo: ''},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule { }
