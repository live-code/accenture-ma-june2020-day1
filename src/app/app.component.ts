import { Component } from '@angular/core';
import { ThemeService } from './core/services/theme.service';

@Component({
  selector: 'app-root',
  template: `    
    <app-navbar 
      [logo]="url"
      color="red"
    ></app-navbar>
    
    <div class="container mt-3">
      <router-outlet></router-outlet>
    </div>
    
    <!--app-footer-->
    
    <hr>
    tema: {{themeService.value}}
  `,
})
export class AppComponent {
  url: number | string = 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/1200px-Angular_full_color_logo.svg.png'

  constructor(public themeService: ThemeService) {

  }

}
