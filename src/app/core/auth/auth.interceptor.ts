import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, distinct, filter, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloned = req;

    if (this.authService.isLogged()) {
      cloned = req.clone({
        setHeaders: {
          Authentication: this.authService.token
        }
      });
    }

    return next.handle(cloned)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 404:
                break;
              case 0:
              case 401:
                // Toast.notification('error')
                this.router.navigateByUrl('login')
                break;
            }
          }
          // === 0=== 0 ====
          return throwError(err);
        })
      );
  }

}


