import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Credentials } from '../../model/credentials';
import { Auth } from './auth';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  data: Auth;
  error: boolean;

  constructor(
    private  http: HttpClient,
    private router: Router
  ) { }

  signin(credetials: Credentials) {
    this.error = false;
    this.http.get<Auth>(`http://localhost:3000/xxxlogin?u=${credetials.username}&p=${credetials.password}`)
      .subscribe(
        res => {
          this.data = res;
          this.router.navigateByUrl('catalog');

          localStorage.setItem('token', this.data.token)
        },
        err => {
          console.log('errore', err)
          this.error = true;
        }
      );
  }

  logout() {
    this.data = null;
    localStorage.removeItem('token');
    this.router.navigateByUrl('login');
  }

  isLogged(): boolean {
    return !!localStorage.getItem('token');
    // return !!this.data;
  }

  get token() {
    // return this.data?.token;
    return localStorage.getItem('token')
  }
}
