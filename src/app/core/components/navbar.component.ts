import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-navbar',
 // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
     <nav 
       class="navbar navbar-expand"
       [ngClass]="{
          'navbar-light bg-light': themeService.value === 'light',
          'navbar-dark bg-dark': themeService.value === 'dark'
       }"
     >
      <a class="navbar-brand">
        <img [src]="logoValue" width="50">
        {{authService.data?.displayName}} 
      </a>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link" routerLink="" [routerLinkActiveOptions]="{ exact: true}" routerLinkActive="bg-warning">Home</a>
          <a class="nav-item nav-link" [routerLink]="'tvmaze'" routerLinkActive="bg-warning">TV MAze</a>
          <a class="nav-item nav-link" *ngIf="authService.isLogged()" routerLink="catalog" routerLinkActive="bg-warning">Catalog</a>
          <a class="nav-item nav-link" routerLink="uikit" routerLinkActive="bg-warning">UIKIT</a>
          <a class="nav-item nav-link" routerLink="settings" routerLinkActive="bg-warning">settings</a>
          <a class="nav-item nav-link" routerLink="login" routerLinkActive="bg-warning">login</a>
          <a class="nav-item nav-link" (click)="authService.logout()">QUIT</a>
        </div>
      </div>
    </nav>
  `,
})
export class NavbarComponent {
  @Input() set logo(val: string) {
    this.logoValue = val;
  }
  logoValue: string;

  @Input() color: string;
/*
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
  }*/

  constructor(
    public themeService: ThemeService,
    public authService: AuthService
  ) {
    console.log(themeService.value)
  }
}
