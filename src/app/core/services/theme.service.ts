import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  value: 'dark' | 'light' = 'dark';

  setTheme(value: 'dark' | 'light') {
    this.value = value;
  }
}
