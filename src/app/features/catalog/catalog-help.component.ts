import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog-help',
  template: `
    <p>
      catalog-help works!
    </p>
    <button routerLink="../">Back to catalog</button>
  `,
  styles: [
  ]
})
export class CatalogHelpComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
