import { Component } from '@angular/core';
import { Device } from '../../model/device';
import { CatalogService } from './services/catalog.service';

@Component({
  selector: 'app-catalog',
  template: `
    <button routerLink="help">goto the help</button>
    <hr>
    <app-card title="SEARCH" [isOpen]="isOpen" (toggle)="isOpen = !isOpen">
      <app-catalog-search
        [active]="catalogService.active"
        (save)="catalogService.save($event)"
        (clear)="catalogService.clear()"
      ></app-catalog-search>
    </app-card>
      
      <app-catalog-list
        [devices]="catalogService.devices"
        [active]="catalogService.active"
        (setActive)="catalogService.setActive($event)"
        (delete)="catalogService.deleteHandler($event)"
        (tab)="catalogService.gotoNext($event)"
      ></app-catalog-list>
      
  `,
})
export class CatalogComponent {
  isOpen = true;

  constructor(public catalogService: CatalogService) {
    this.catalogService.getAll();
  }
}
