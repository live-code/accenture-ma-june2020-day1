import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogComponent } from './catalog.component';
import { CatalogSearchComponent } from './components/catalog-search.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { IconComponent } from './components/icon.component';
import { PriceComponent } from './components/price.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { CatalogHelpComponent } from './catalog-help.component';



@NgModule({
  declarations: [
    // feature: catalog
    CatalogComponent,
    CatalogSearchComponent,
    CatalogListComponent,
    IconComponent,
    PriceComponent,
    CatalogHelpComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent },
      { path: 'help', component: CatalogHelpComponent },

    ])
  ]
})
export class CatalogModule { }
