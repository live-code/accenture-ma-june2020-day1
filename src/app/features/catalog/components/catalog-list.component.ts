import { AfterViewInit, Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Device } from '../../../model/device';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-catalog-list',
  template: `
      <div *ngIf="devices">
        {{devices?.length}} prodotti
      </div>
  
      <div
        *ngFor="let device of devices"
        (click)="setActive.emit(device)"
        class="list-group-item"
        [ngClass]="{
            'active': device.id === active?.id
          }"
      >
        <app-icon [os]="device.os"></app-icon>
        <span>{{device.label}}</span>
        <app-gmap *ngIf="device.madeIn" [value]="device.madeIn"></app-gmap>
  
        <div class="pull-right">
          <app-price [value]="device.price"></app-price>
          <i 
            class="fa fa-trash" 
            (click)="deleteHandler(device, $event)"></i>
        </div>
      </div>
  `,
})
export class CatalogListComponent implements AfterViewInit, OnDestroy {
  @Input() devices: Device[];
  @Input() active: Device;
  @Output() setActive: EventEmitter<Device> = new EventEmitter();
  @Output() delete: EventEmitter<Device> = new EventEmitter();
  @Output() tab: EventEmitter<boolean> = new EventEmitter();

  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngAfterViewInit() {
    this.document.addEventListener('keydown', this.emitTab);
  }

  emitTab = (e: KeyboardEvent) => {
    if (e.code === 'Tab') {
      this.tab.emit(e.shiftKey);
    }
  }


  deleteHandler(device: Device, event: MouseEvent) {
    event.stopPropagation();
    this.delete.emit(device);
  }

  ngOnDestroy(): void {
    this.document.removeEventListener('keydown', this.emitTab);
  }
}

