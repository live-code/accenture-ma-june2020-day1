import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Device } from '../../../model/device';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-catalog-search',
  template: `
    <form #f="ngForm" (submit)="saveHandler(f.value)">
      <div class="form-group">
        <input type="text" #labelInput name="label" 
               [ngModel]="active?.label" class="form-control">
      </div>
      <div class="form-group">
        <input type="text" name="price" [ngModel]="active?.price" class="form-control">
      </div>


      <div class="btn-group btn-block">
        <button type="submit" class="btn btn-primary">
          {{active?.id ? 'EDIT' : 'ADD'}}
        </button>

        <button 
          class="btn btn-outline-primary" 
          type="button" (click)="clearHandler()">Clear</button>
      </div>
    </form>
  `,
})
export class CatalogSearchComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('labelInput', { static: true }) labelInput: ElementRef<HTMLInputElement>;
  @ViewChild('f', { static: true } ) form: NgForm;
  @Input() active: Device;
  @Output() save: EventEmitter<Device> = new EventEmitter<any>()
  @Output() clear: EventEmitter<void> = new EventEmitter<any>()

  ngOnInit() {
    this.labelInput.nativeElement.focus();
  }

  ngOnChanges(changes: SimpleChanges) {
    const { active, color } = changes;

    if (active && !active.currentValue.id) {
      this.form.reset();
    }
  }

  ngAfterViewInit(): void {

  }

  saveHandler(formData: Device) {
    this.save.emit(formData);
  }

  clearHandler() {
    this.clear.emit();
  }
}
