import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <i
      class="fa"
      [ngClass]="{
            'fa-android': os === 'android',
            'fa-apple': os === 'ios'
          }"
    ></i>
    {{render()}}
  `,
})
export class IconComponent {
  @Input() os: string;

  render() {
    console.log('render: icon')
  }
}
