import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-price',
  template: `
    <span [style.color]="value > 500 ? 'red' : null">
    {{value}}
    </span>
  `,
})
export class PriceComponent  {
  @Input() value: number;

}
