import { Injectable } from '@angular/core';
import { Device } from '../../../model/device';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  devices: Device[];
  active: Device = {} as Device;

  constructor(
    private http: HttpClient,
  ) {}

  getAll() {
    this.http.get<Device[]>('http://localhost:3000/devices')
      .subscribe(result => {
        this.devices = result;
      });
  }
  deleteHandler(device: Device) {
    this.http.delete(`http://localhost:3000/devices/${device.id}`)
      .subscribe(() => {
        const index = this.devices.findIndex(item => item.id === device.id);
        this.devices.splice(index, 1);

        if (this.active.id === device.id) {
          this.clear();
        }
      });
  }

  save(formData: Device) {
    if (this.active?.id) {
      this.edit(formData);
    } else {
      this.add(formData);
    }
  }

  edit(formData: Device) {
    this.http.patch<Device>(`http://localhost:3000/devices/${this.active.id}`, formData)
      .subscribe(() => {
        const index = this.devices.findIndex(item => item.id === this.active.id);
        // this.devices[index] = Object.assign({}, this.active, device);
        this.devices[index] = { ...this.active, ...formData }; // object spread operator
      });
  }

  add(formData: Device) {
    this.http.post<Device>(`http://localhost:3000/devices/`, formData)
      .subscribe(res => {
        this.devices.push(res);
        this.clear();
      });
  }

  setActive(device: Device) {
    this.active = device;
  }

  clear() {
    this.active = {} as Device;
  }

  gotoNext(isShiftPressed: boolean) {
    const index = this.devices.findIndex(d => d.id === this.active.id)

    if (isShiftPressed) {
      this.active = index > 0 ?
        this.devices[index - 1] :
        this.devices[this.devices.length - 1];
    } else {
      this.active = index < this.devices.length - 1 ?
        this.devices[index + 1] :
        this.devices[0];
    }

  }

}
