import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lostpass',
  template: `
    <p>
      <input type="text" placeholder="hai perso la pass?">
    </p>
    
    <button routerLink="../signin">go to login</button>
  `,
  styles: [
  ]
})
export class LostpassComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
