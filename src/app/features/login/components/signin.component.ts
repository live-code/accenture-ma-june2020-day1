import { Component, OnInit } from '@angular/core';
import { Credentials } from '../../../model/credentials';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'app-signin',
  template: `
    {{authService.data | json}}
    
    <div class="alert alert-danger" *ngIf="authService.error">
      errore
    </div>
    <form #f="ngForm" (submit)="login(f.value)">
      <input type="text" [ngModel] name="username">
      <input type="password" [ngModel] name="password">
      <button type="submit">SIGNIN</button>
    </form>
    
    <hr>
    <button routerLink="../lostpass">Lost Pass?</button>
  `,
})
export class SigninComponent {

  constructor(public  authService: AuthService) { }


  login(data: Credentials) {
    this.authService.signin(data)
  }
}
