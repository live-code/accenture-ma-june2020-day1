import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { SigninComponent } from './components/signin.component';
import { RegistrationComponent } from './components/registration.component';
import { LostpassComponent } from './components/lostpass.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: 'registration', component: RegistrationComponent },
      { path: 'lostpass', component: LostpassComponent },
      { path: 'signin', component: SigninComponent },
      { path: '', redirectTo: 'signin' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
