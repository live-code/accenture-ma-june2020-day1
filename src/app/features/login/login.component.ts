import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `
    <button routerLink="signin">signin</button>
    <button routerLink="registration">registration</button>
    <button routerLink="lostpass">lostpass</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
