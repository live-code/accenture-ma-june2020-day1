import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LostpassComponent } from './components/lostpass.component';
import { RegistrationComponent } from './components/registration.component';
import { SigninComponent } from './components/signin.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [LoginComponent, LostpassComponent, RegistrationComponent, SigninComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule
  ]
})
export class LoginModule { }
