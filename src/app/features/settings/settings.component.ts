import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <button (click)="themeService.setTheme('light')">light</button>
    <button (click)="themeService.setTheme('dark')">dark</button>
  `,
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) {
  }

  ngOnInit(): void {
  }

}
