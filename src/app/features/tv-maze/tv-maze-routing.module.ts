import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UikitComponent } from '../uikit/uikit.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: UikitComponent}
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class TvMazeRoutingModule {

}
