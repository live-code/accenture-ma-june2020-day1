import { Component } from '@angular/core';
import { Series } from '../../model/series';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tvmaze-demo',
  template: `

    <div class="container">
      <div class="alert alert-danger" *ngIf="f.invalid && f.touched">Form invalid</div>

      <form #f="ngForm" (submit)="search(f, inputText)">

        <div class="input-group mb-3">
          <div class="input-group-prepend">
          <span
            class="input-group-text"
            [style.background-color]="inputText.valid ? 'lightgreen' : 'white'"
          >
              <i
                class="fa"
                [ngClass]="{
                  'fa-check': inputText.valid,
                  'fa-exclamation-triangle': inputText.invalid
                }"
                [style.color]="inputText.valid ? 'green' : 'red'"

              ></i>
            </span>
          </div>

          <input
            #inputText="ngModel"
            [ngModel]
            type="text"
            name="text"
            class="form-control"
            placeholder="Search series"
            required
            minlength="3"
          >
        </div>



        <label *ngIf="inputText.errors?.required && inputText.dirty">Campo Obbligatorio</label>
        <label *ngIf="inputText.errors?.minlength && inputText.dirty">Almeno 3 caratteri</label>


        <button
          [disabled]="f.invalid"
          type="submit" class="btn btn-primary btn-block">Search</button>
      </form>

      <hr>
      <em>{{series?.length}} results</em>

      <li class="list-group-item"
          *ngFor="let item of series"
          [ngClass]="{
          'ended': item.show.status === 'Ended', 
          'pending': item.show.status === 'Running'
        }"
      >
        <img
          [src]="item.show.image ? item.show.image.medium : 'https://screenshotlayer.com/images/assets/placeholder.png'"
          width="50"
          alt=""
        >
        {{item.show.name}}

        <div class="pull-right">
          <i class="fa fa-2x fa-arrow-circle-right" (click)="openUrl(item.show.url)"></i>
        </div>
      </li>
    </div>
  
  `
})
export class TvMazeComponent {
  series: Series[];

  constructor(private http: HttpClient) {
  }

  search(form: NgForm, input: any) {
    console.log(input)
    this.http.get<Series[]>(`http://api.tvmaze.com/search/shows?q=${form.value.text}`)
      .subscribe(result => {
        this.series = result;
      });
  }
  openUrl(url: string) {
    window.open(url);
  }
}
