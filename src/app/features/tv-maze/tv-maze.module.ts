import { NgModule } from '@angular/core';
import { TvMazeComponent } from './tv-maze.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    TvMazeComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: TvMazeComponent}
    ])
  ]
})
export class TvMazeModule {

}
