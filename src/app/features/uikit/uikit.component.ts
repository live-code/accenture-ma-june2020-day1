import { Component, OnInit } from '@angular/core';
import { City, Country } from '../../shared/model/country';

@Component({
  selector: 'app-uikit',
  template: `
    <app-tabbar 
      [items]="countries"
      [active]="activeCountry"
      (tabClick)="selectCountryHandler($event)"
    ></app-tabbar>
    
    <app-tabbar
      [items]="activeCountry?.cities"
      [active]="activeCity"
      labelField="name"
      (tabClick)="selectCityHandler($event)"
    ></app-tabbar>
   
    <app-gmap 
      *ngIf="activeCity" 
      [value]="activeCity?.name"
      [zoom]="zoom"
    ></app-gmap>
    <br>
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="zoom = zoom - 1">-</button>
    
    
 
    <app-card 
      label="pippo" 
      headerCls="bg-warning"
      icon="fa fa-link"
      [isOpen]="opened"
      (toggle)="opened = !opened"
      (iconClick)="openUrl('http://www.pippo.com')"
    >
      <input type="text">
      <input type="text">
      <input type="text">
    </app-card>
    
    <app-card 
      label="CICCIO" 
      icon="fa fa-bluetooth"
      [marginTop]="5"
      [isOpen]="opened"
      (toggle)="opened = !opened"
      (iconClick)="doSomething()"
    >
      <div class="row">
        <div class="col">
          <app-card title="left">....</app-card>
        </div>
        <div class="col">
          <app-card title="right">....</app-card>
        </div>
      </div>
    </app-card>
  `,
})
export class UikitComponent {
  opened = true;
  countries: Country[] = [
    {
      id: 11,
      label: 'Japan',
      cities: [
        { id: 10, name: 'Tokyo'},
        { id: 11, name: 'Kyoto'},
      ]
    },
    {
      id: 21,
      label: 'Italy',
      cities: [
        { id: 10, name: 'Rome'},
        { id: 11, name: 'Milan'},
        { id: 12, name: 'Trieste'},
      ]
    },
    {
      id: 13,
      label: 'China',
      cities: [
        { id: 10, name: 'Pechino'},
      ]
    }
  ]
  activeCountry: Country;
  activeCity: City;
  zoom = 5;

  constructor() {
    this.selectCountryHandler(this.countries[0]);
  }

  doSomething() {
    console.log('do something')
  }

  openUrl(url: string) {
    window.open(url);
  }

  selectCountryHandler(c: Country) {
    this.activeCountry = c;
    this.activeCity = this.activeCountry.cities[0];
  }

  selectCityHandler(city: City) {
    this.activeCity = city;
    this.zoom = 1;
  }
}
