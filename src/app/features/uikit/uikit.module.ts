import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UikitComponent } from './uikit.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { TvMazeRoutingModule } from '../tv-maze/tv-maze-routing.module';



@NgModule({
  declarations: [
    UikitComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    TvMazeRoutingModule
  ]
})
export class UikitModule { }
