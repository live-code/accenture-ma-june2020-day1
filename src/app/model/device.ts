export interface Device {
  id: number;
  label: string;
  memory: number;
  os: string;
  madeIn: string;
  price: number;
  releaseDate: number;
}
