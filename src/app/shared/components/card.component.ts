import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * Un pannello con title e body
 */
@Component({
  selector: 'app-card',
  template: `
    <div class="card" [ngClass]="'mt-' + marginTop">
      
      <div 
        class="card-header" 
        [ngClass]="headerCls"
        (click)="toggle.emit()"
      >
        {{label}} 
        <div class="pull-right">
          <i 
            *ngIf="icon" 
            [class]="icon"
            (click)="iconClick.emit()"
          ></i>
        </div>
      </div>
      
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent  {
  @Input() isOpen: boolean;

  @Input() label = 'WIDGET';
  /**
   * l'icona dell'header
   */
  @Input() icon: string;
  /**
   * Lo stile dell'header
   */
  @Input() headerCls = 'bg-dark text-white';
  /**
   * Il margine top dell'header
   */
  @Input() marginTop: 0 | 1 | 2 | 3 | 4 | 5 = 3;

  @Output() iconClick = new EventEmitter();

  @Output() toggle = new EventEmitter();

}
