import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gmap',
  template: `
    <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + value + '&zoom=' + zoom + '&size=300x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
  `,
})
export class GmapComponent  {
  @Input() value: string;
  @Input() zoom = 5;
}
