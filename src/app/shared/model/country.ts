export interface Country {
  id: number;
  label: string;
  cities: City[];
}

export interface City {
  id: number;
  name: string;
}
