import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { GmapComponent } from './components/gmap.component';
import { TabbarComponent } from './components/tabbar.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CardComponent,
    GmapComponent,
    TabbarComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    GmapComponent,
    TabbarComponent,
    FormsModule
  ]
})
export class SharedModule { }
